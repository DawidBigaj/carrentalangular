import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { CustomersComponent } from './customers/customers.component';
import { CustomerDetailComponent } from './customer-detail/customer-detail.component';
import {CustomerService} from './customer.service';
import { AppRoutingModule } from './/app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';
import { CarsComponent } from './cars/cars.component';
import {CarService} from './car.service';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { CarDetailComponent } from './car-detail/car-detail.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { ReservationComponent } from './reservation/reservation.component';
import {ReservationService} from './reservation.service';
import { ModalModule } from 'ngx-bootstrap/modal';
import { RentsComponent } from './rents/rents.component';
import {RentService} from './rent.service';
import {ButtonsModule} from 'ngx-bootstrap';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2OrderModule } from 'ng2-order-pipe';
import {AdminguardGuard} from './adminguard.guard';
import {UserguardGuard} from './userguard.guard';
import { UserPanelComponent } from './user-panel/user-panel.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomersComponent,
    CustomerDetailComponent,
    CarsComponent,
    CarDetailComponent,
    LoginFormComponent,
    AdminPanelComponent,
    ReservationComponent,
    RentsComponent,
    UserPanelComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    Ng2SearchPipeModule,
    Ng2OrderModule,
    HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, {dataEncapsulation: false}),
    BsDatepickerModule.forRoot(), ModalModule.forRoot(), ButtonsModule.forRoot(),
  ],
  providers: [CustomerService, CarService, ReservationService, RentService, AdminguardGuard, UserguardGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }

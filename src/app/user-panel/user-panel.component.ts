import {AfterContentInit, AfterViewInit, Component, OnInit} from '@angular/core';
import {CustomerService} from '../customer.service';

@Component({
  selector: 'app-user-panel',
  templateUrl: './user-panel.component.html',
  styleUrls: ['./user-panel.component.css']
})
export class UserPanelComponent implements OnInit, AfterContentInit {
  customerId: number;

  constructor(private userService: CustomerService) { }

  ngOnInit() {
  }

  ngAfterContentInit(): void {
    this.customerId = this.userService.getCustomerId();
  }

}

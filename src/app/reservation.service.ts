import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Rent} from './rent';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ReservationService {
  private reservationUrl = 'api/reservations';
  constructor(private http: HttpClient) { }

  getReservations(): Observable<Rent[]> {
    return this.http.get<Rent[]>(this.reservationUrl);
  }

  addReservation(rent: Rent): Observable<Rent> {
    return this.http.post<Rent>(this.reservationUrl, rent, httpOptions);
  }

  deleteReservation(reservation: Rent| number): Observable<Rent> {
    const id = typeof reservation === 'number' ? reservation : reservation.id;
    const url = `${this.reservationUrl}/${id}`;

    return this.http.delete<Rent>(url, httpOptions);
  }
}

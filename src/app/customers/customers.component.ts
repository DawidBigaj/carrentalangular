import {Component, OnInit, TemplateRef} from '@angular/core';
import { Customer } from '../customer';
import { CustomerService } from '../customer.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})

export class CustomersComponent implements OnInit {
  customers: Customer[];
  customer: Customer;
  modalRef: BsModalRef;
  key = 'id';
  reverse = false;
  warning: string;

  constructor(private customerService: CustomerService, private modalService: BsModalService) { }

  ngOnInit() {
    this.getCustomers();
  }

  getCustomers(): void {
    this.customerService.getCustomers().subscribe(customers => this.customers = customers);
  }

  add(name: string, surname: string, email: string, creditCardNumber: string, password: string, repeat: string): void {
    name = name.trim();
    surname = surname.trim();
    email = email.trim();
    creditCardNumber = creditCardNumber.trim();
    password = password.trim();
    repeat = repeat.trim();

    if (!name || !surname || !email || !creditCardNumber) {
      this.warning = 'Please fill out all required fields';
      return;
    }
    if (password !== repeat) {
      this.warning = ' Password and confirmation password do not match.';
      return;
    }
    this.customerService.addCustomer(new Customer(name, surname, email, creditCardNumber, password))
      .subscribe(customer => {this.customers.push(customer);
    });
    this.modalRef.hide();
  }

  delete(customer: Customer): void {
    this.customers = this.customers.filter(c => c !== customer);
    this.customerService.deleteCustomer(customer).subscribe();
  }

  openModal(template: TemplateRef<any>) {
    this.warning = null;
    this.modalRef = this.modalService.show(template);
  }

  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }
}

export class Car {
  id: number;
  mark: string;
  model: string;
  description: string;
  colour: string;
  productionDate: string;
  retirementDate: string;
  mileage: string;
  servicePeriod: string;


  constructor(id: number, mark: string, model: string, description: string, colour: string, productionDate: string, retirementDate: string
              , mileage: string, servicePeriod: string) {
    this.id = id;
    this.mark = mark;
    this.model = model;
    this.description = description;
    this.colour = colour;
    this.productionDate = productionDate;
    this.retirementDate = retirementDate;
    this.mileage = mileage;
    this.servicePeriod = servicePeriod;
  }
}

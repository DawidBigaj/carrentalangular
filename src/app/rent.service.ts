import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Rent} from './rent';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class RentService {
  private rentUrl = 'api/rents';
  constructor(private http: HttpClient) { }

  getRents(): Observable<Rent[]> {
    return this.http.get<Rent[]>(this.rentUrl);
  }

  addRent(rent: Rent): Observable<Rent> {
    return this.http.post<Rent>(this.rentUrl, rent, httpOptions);
  }

  deleteRent(rent: Rent| number): Observable<Rent> {
    const id = typeof rent === 'number' ? rent : rent.id;
    const url = `${this.rentUrl}/${id}`;

    return this.http.delete<Rent>(url, httpOptions);
  }
}

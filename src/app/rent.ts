export class Rent {
  id: number;
  idCustomer: number;
  idCar: number;
  rentDate: string;
  returnDate: string;
  gps: boolean;
  wifi: boolean;


  constructor(idCustomer: number, idCar: number, rentDate: string, returnDate: string, gps: boolean, wifi: boolean) {
    this.idCustomer = idCustomer;
    this.idCar = idCar;
    this.rentDate = rentDate;
    this.returnDate = returnDate;
    this.gps = gps;
    this.wifi = wifi;
  }
}

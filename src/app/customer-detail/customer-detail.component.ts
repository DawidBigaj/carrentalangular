import {Component, OnInit, Input, TemplateRef} from '@angular/core';
import { Customer } from '../customer';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { CustomerService } from '../customer.service';
import {Rent} from '../rent';
import {RentService} from '../rent.service';
import {CarService} from '../car.service';
import {Car} from '../car';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.css']
})
export class CustomerDetailComponent implements OnInit {
  customer: Customer;
  password: string;
  confirmation: string;
  rents: Rent[];
  cars: Car[];
  key = 'id';
  reverse = false;
  tab1 = true;
  warning: string;
  modalRef: BsModalRef;

  constructor(private route: ActivatedRoute, private location: Location, private customerService: CustomerService
              , private rentService: RentService, private carService: CarService, private modalService: BsModalService) { }

  ngOnInit() {
    this.getCustomer();
    this.getCustomerCars();
  }

  getCustomer(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.customerService.getCustomer(id).subscribe(customer => { this.customer = customer;
      this.password = this.customer.password;
    });
  }
  getCustomerCars(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.rentService.getRents()
      .subscribe(rents => {
        this.rents = rents.filter(r => r.idCustomer === id);
        this.carService.getCars().subscribe(cars => this.cars = cars
          .filter(c => this.rents.map(r => r.idCar).includes(c.id)));
      });
  }

  goBack(): void {
    this.location.back();
  }

  save(template: TemplateRef<any>): void {
    if (this.password !== this.customer.password && this.password !== this.confirmation) {
      this.warning = ' Password and confirmation password do not match.';
    } else {
      this.customer.password = this.password;
      this.customerService.updateCustomer(this.customer).subscribe();
      this.openModal(template);
    }
  }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }

}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomersComponent } from './customers/customers.component';
import { CustomerDetailComponent } from './customer-detail/customer-detail.component';
import {CarsComponent} from './cars/cars.component';
import {CarDetailComponent} from './car-detail/car-detail.component';
import {LoginFormComponent} from './login-form/login-form.component';
import {AdminPanelComponent} from './admin-panel/admin-panel.component';
import {ReservationComponent} from './reservation/reservation.component';
import {RentsComponent} from './rents/rents.component';
import {AdminguardGuard} from './adminguard.guard';
import {UserguardGuard} from './userguard.guard';

const routes: Routes = [
  { path: 'customers', canActivate: [AdminguardGuard], component: CustomersComponent },
  { path: 'cars', canActivate: [AdminguardGuard],  component: CarsComponent },
  { path: '', redirectTo: 'login-form', pathMatch: 'full' },
  { path: 'customer-detail/:id', component: CustomerDetailComponent},
  { path: 'car-detail/:id', canActivate: [AdminguardGuard], component: CarDetailComponent},
  { path: 'login-form', component: LoginFormComponent},
  { path: 'admin-panel', canActivate: [AdminguardGuard],  component: AdminPanelComponent},
  { path: 'reservation/:id', canActivate: [UserguardGuard], component: ReservationComponent},
  { path: 'rent', canActivate: [AdminguardGuard],  component: RentsComponent}
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)],
})

export class AppRoutingModule {
}

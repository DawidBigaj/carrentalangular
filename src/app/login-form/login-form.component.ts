import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CustomerService} from '../customer.service';
import {Customer} from '../customer';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit, AfterViewInit {
  customers: Customer[];
  warning: string;
  i: number;

  constructor(private router: Router, private userService: CustomerService) { }

  ngOnInit() {
    this.userService.getCustomers().subscribe(customers => this.customers = customers);
  }

  loginUser(email: string, password: string) {
    if (email === 'admin' && password === 'admin') {
      this.userService.setAdminLoggedIn();
      this.router.navigate(['customers']);
    } else if (this.userValidation(email, password)) {
      this.userService.setUserLoggedIn(this.customers[this.i].id);
      this.userService.setCustomerId(this.customers[this.i].id);
      this.router.navigate(['customer-detail/' + this.customers[this.i].id.toString()]);
    } else {
      this.warning = 'Email or password are incorrect';
    }
  }
  userValidation (email: string, password: string) {
    for (this.i = 0; this.i < this.customers.length; this.i++) {
      if (this.customers[this.i].email === email && this.customers[this.i].password === password) {
        return true;
      }
    }
    return false;
  }

  ngAfterViewInit(): void {
     this.userService.clear();
  }

}

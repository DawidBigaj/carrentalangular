import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginFormComponent } from './login-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Router} from '@angular/router';
import {CustomerService} from '../customer.service';
import {Customer} from '../customer';
import {defer} from 'rxjs/observable/defer';
import {of} from 'rxjs/observable/of';

describe('LoginFormComponent', () => {
  let component: LoginFormComponent;
  let fixture: ComponentFixture<LoginFormComponent>;
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
  const customerServiceSpy = jasmine.createSpyObj('CustomerService', ['getCustomers', 'clear', 'setAdminLoggedIn', 'setUserLoggedIn'
    , 'setCustomerId']);
  const customers = [
    {id: 11, name: 'Viki', surname: 'Hush', email: 'vhush0@hubpages.com', creditCardNumber: '3563875524554340', password: 'p1'},
    {id: 12, name: 'June', surname: 'Titheridge', email: 'jtitheridge1@blog.com', creditCardNumber: '630461812348718564', password: 'p2'},
  ] as Customer[];
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule],
      declarations: [ LoginFormComponent ],
      providers: [
        {provide: Router, useValue: routerSpy},
        {provide: CustomerService, useValue: customerServiceSpy}
      ]
    })
    .compileComponents();

    customerServiceSpy.getCustomers.and.returnValue(asyncData(of(customers)));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should return true when email and password are correct', () => {
    component.customers = customers;
    expect(component.userValidation('vhush0@hubpages.com', 'p1')).toEqual(true, 'expected value');
  });
  it('should return false when email or password are incorrect', () => {
    component.customers = customers;
    expect(component.userValidation('vhush0@hubpages.com', 'p2')).toEqual(false, 'expected value');
  });
  it('should show warning when email or password are incorrect', () => {
    component.customers = customers;
    component.loginUser('vhush0@hubpages.com', 'p2');
    expect(component.warning).toBe('Email or password are incorrect');
    expect(routerSpy.navigate).toHaveBeenCalledTimes(0);
  it('should navigate to customers page when email and password of admin are correct', () => {
    component.loginUser('admin', 'admin');
    expect(routerSpy.navigate).toHaveBeenCalledWith(['customers']);
  });
  it('should navigate to customer panel when email and password of user are correct', () => {
    component.customers = customers;
    component.loginUser('vhush0@hubpages.com', 'p1');
    expect(routerSpy.navigate).toHaveBeenCalledWith(['customer-detail/11']);
  });
  });
  function asyncData<T>(data: T) {
    return defer(() => Promise.resolve(data));
  }
});

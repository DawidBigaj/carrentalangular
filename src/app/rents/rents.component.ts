import {Component, OnInit, TemplateRef} from '@angular/core';
import {ReservationService} from '../reservation.service';
import {Rent} from '../rent';
import {RentService} from '../rent.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {FormBuilder, FormGroup} from '@angular/forms';
import {CarService} from '../car.service';
import {Car} from '../car';

@Component({
  selector: 'app-rents',
  templateUrl: './rents.component.html',
  styleUrls: ['./rents.component.css']
})
export class RentsComponent implements OnInit {
  rents: Rent[];
  reservations: Rent[];
  endedRents: Rent[];
  modalRef: BsModalRef;
  myForm: FormGroup;
  rentId: number;
  carId: number;
  tab = 1;
  key = 'id';
  reverse = false;

  constructor(private reservationService: ReservationService, private rentService: RentService, private carService: CarService
              , private modalService: BsModalService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getRents();
    this.getReservations();
    this.myForm = this.formBuilder.group({
      wifi: false,
      gps: false,
      carId: 0,
      customerId: 0,
      rentDate: '',
      returnDate: '',
      id: 0,
    });
  }

  getReservations(): void {
    this.reservationService.getReservations().subscribe(reservations => this.reservations = reservations);
  }

  getRents(): void {
    this.rentService.getRents().subscribe(rents => {this.rents = rents, this.getEndedRents();
    });
  }

  getEndedRents(): void {
    this.endedRents = this.rents.filter(r => new Date(r.returnDate).getTime() < Date.now());
    this.rents = this.rents.filter(r => new Date(r.returnDate).getTime() >= Date.now());
  }

  add(id: number, carId: number, customerId: number, rentDate: string, returnDate: string, gps: boolean, wifi: boolean): void {
    this.rentService.addRent(new Rent(carId, customerId, rentDate, returnDate, gps, wifi))
      .subscribe(rent => {this.rents.push(rent);
      });
    this.deleteReservation(id);
  }

  deleteReservation(id: number): void {
    this.reservations = this.reservations.filter(r => r.id !== id);
    this.reservationService.deleteReservation(id).subscribe();
  }
  returnRent(): void {
    let sp = '';
    const m = (<HTMLInputElement>document.getElementById('mileage')).value;
    if ((<HTMLInputElement>document.getElementById('serviceDate')).value.length === 0) {
      sp = (<HTMLInputElement>document.getElementById('serviceDate')).placeholder;
    } else {
      sp = (<HTMLInputElement>document.getElementById('serviceDate')).value;
    }
    this.endedRents = this.endedRents.filter(r => r.id !== this.rentId);
    this.rentService.deleteRent(this.rentId).subscribe();
    this.carService.getCar(this.carId).subscribe(c => this.carService.updateCar(new Car(c.id, c.mark, c.model
      , c.description, c.colour, c.productionDate, c.retirementDate, m, sp)).subscribe(() => this.carService.getCars()));
  }

  openModal(template: TemplateRef<any>, reservation: Rent) {
    if (reservation !== null) {
      this.myForm.controls['id'].setValue(reservation.id);
      this.myForm.controls['carId'].setValue(reservation.idCar);
      this.myForm.controls['customerId'].setValue(reservation.idCustomer);
      this.myForm.controls['rentDate'].setValue(reservation.rentDate);
      this.myForm.controls['returnDate'].setValue(reservation.returnDate);
      this.myForm.controls['gps'].setValue(reservation.gps);
      this.myForm.controls['wifi'].setValue(reservation.wifi);
    }
    this.modalRef = this.modalService.show(template);
  }

  openDeleteModal(template: TemplateRef<any>, idRent: number, idCar: number) {
    if (idCar !== null) {
      this.carService.getCar(idCar).subscribe(c => {
        this.rentId = idRent;
        this.carId = idCar;
        document.getElementById('mileage').setAttribute('value', c.mileage);
        document.getElementById('serviceDate').setAttribute('placeholder', c.servicePeriod);
      });
    }
    this.modalRef = this.modalService.show(template);
  }

  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }
}

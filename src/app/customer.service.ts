import { Injectable } from '@angular/core';
import { Observable} from 'rxjs/Observable';
import { Customer } from './customer';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class CustomerService {
  private customersUrl = 'api/customers';
  private isUserLoggedIn: boolean;
  private isAdminLoggedIn: boolean;
  private customerId: number;
  constructor(private http: HttpClient) {
    this.clear();
  }
  clear() {
    this.isUserLoggedIn = false;
    this.isAdminLoggedIn = false;
    this.customerId = 0;
  }
  setUserLoggedIn(customerId: number) {
    this.isUserLoggedIn = true;
    this.isAdminLoggedIn = false;
    this.customerId  = customerId;
  }

  getUserLoggedIn() {
    return this.isUserLoggedIn;
  }

  setAdminLoggedIn() {
    this.isUserLoggedIn = false;
    this.isAdminLoggedIn = true;
    this.customerId = 0;
  }

  getAdminLoggedIn() {
    return this.isAdminLoggedIn;
  }
    setCustomerId(id: number) {
    this.customerId = id;
  }
  getCustomerId() {
    return this.customerId;
  }


  getCustomers(): Observable<Customer[]> {
    return this.http.get<Customer[]>(this.customersUrl);
  }

  getCustomer(id: number): Observable<Customer> {
    const url = `${this.customersUrl}/${id}`;
    return this.http.get<Customer>(url);
  }

  updateCustomer(customer: Customer): Observable<any> {
    return this.http.put(this.customersUrl, customer, httpOptions);
  }

  addCustomer(customer: Customer): Observable<Customer> {
    return this.http.post<Customer>(this.customersUrl, customer, httpOptions);
  }

  deleteCustomer(customer: Customer | number) {
    const id = typeof customer === 'number' ? customer : customer.id;
    const url = `${this.customersUrl}/${id}`;

    return this.http.delete<Customer>(url, httpOptions);
  }

}

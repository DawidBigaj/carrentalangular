// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
//
// import { CarsComponent } from './cars.component';
// import {CarService} from '../car.service';
// import {BsModalService} from 'ngx-bootstrap';
// import {Car} from '../car';
//
// describe('CarsComponent', () => {
//   let component: CarsComponent;
//   let fixture: ComponentFixture<CarsComponent>;
//   let carServiceSpy: jasmine.SpyObj<CarService>;
//   let modelServiceSpy: jasmine.SpyObj<BsModalService>;
//
//   // beforeEach(async(() => {
//   //   const spyCarService = jasmine.createSpyObj('CarService', ['getCars']);
//   //   const spyModalService = jasmine.createSpyObj('BsModalService');
//   //   TestBed.configureTestingModule({
//   //     declarations: [ CarsComponent ],
//   //     providers: [CarsComponent, { provide: CarService, useValue: spyCarService }, { provide: BsModalService, useValue: spyModalService }]
//   //   })
//   //   .compileComponents();
//   // }));
//
//   beforeEach(() => {
//     fixture = TestBed.createComponent(CarsComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//     component = TestBed.get(CarsComponent);
//     carServiceSpy = TestBed.get(CarService);
//     modelServiceSpy = TestBed.get(BsModalService);
//   });
//
//   it('should create', () => {
//     const car = new Car(11, 'Audi', '5 Series', 'none', 'red', '04/12/2007', '07/12/2023'
//       , '200000', '04/11/2018');
//     const cars = new Array<Car>();
//     cars.push(car);
//     carServiceSpy.getCars.and.returnValue(cars);
//     expect(component.getCars).toBe(cars, 'service returned wrong car');
//     //expect(component).toBeTruthy();
//   });
// });

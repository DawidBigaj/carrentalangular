import {Component, OnInit, TemplateRef} from '@angular/core';
import { Car } from '../car';
import {CarService} from '../car.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {
  cars: Car[];
  modalRef: BsModalRef;
  key = 'id';
  reverse = false;
  warning: string;
  constructor(private carService: CarService, private modalService: BsModalService) { }

  ngOnInit() {
    this.getCars();
  }

  getCars(): void {
    this.carService.getCars().subscribe(cars => this.cars = cars);
  }

  add(mark: string, model: string, description: string, colour: string, productionDate: string, retirementDate: string
    , mileage: string, servicePeriod: string): void {
    mark = mark.trim();
    model = model.trim();
    description = description.trim();
    colour = colour.trim();
    productionDate = productionDate.trim();
    retirementDate = retirementDate.trim();
    mileage = mileage.trim();
    servicePeriod = servicePeriod.trim();

    if (!mark || !model || !description || !colour || !mileage || !servicePeriod || !productionDate || !retirementDate || !mileage
      || !servicePeriod) {
      this.warning = 'Please fill out all required fields';
      return;
    }
    this.carService.addCar(new Car(null, mark, model, description, colour, productionDate, retirementDate, mileage, servicePeriod))
      .subscribe(car => {this.cars.push(car);
    });
    this.modalRef.hide();
  }

  delete(car: Car): void {
    this.cars = this.cars.filter(c => c !== car);
    this.carService.deleteCar(car).subscribe();
  }

  openModal(template: TemplateRef<any>) {
    this.warning = null;
    this.modalRef = this.modalService.show(template);
  }

  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }

}

export class Customer {
  id: number;
  name: string;
  surname: string;
  email: string;
  creditCardNumber: string;
  password: string;

  constructor(name: string, surname: string, email: string, creditCardNumber: string, password: string) {
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.creditCardNumber = creditCardNumber;
    this.password = password;
  }
}

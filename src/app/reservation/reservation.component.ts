import {Component, OnInit, TemplateRef} from '@angular/core';
import {ReservationService} from '../reservation.service';
import {Rent} from '../rent';
import {Car} from '../car';
import {CarService} from '../car.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {RentService} from '../rent.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {
  cars: Car[];
  reservations: Rent[];
  rents: Rent[];
  carsCopy: Car[];
  modalRef: BsModalRef;
  key = 'id';
  reverse = false;
  rentDateLabel = '';
  returnDateLabel = '';
  constructor(private reservationService: ReservationService, private carService: CarService, private modalService: BsModalService
              , private rentService: RentService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.getReservations();
    this.getRents();
    this.getCars();
  }

  getCars(): void {
    this.carService.getCars().subscribe(cars => { this.carsCopy = cars;
    });
  }

  getReservations(): void {
    this.reservationService.getReservations().subscribe(reservations => this.reservations = reservations);
  }
  getRents(): void {
    this.rentService.getRents().subscribe(rents => this.rents = rents);
  }

  add(carId: number, templateError: TemplateRef<any>, templateSuccess: TemplateRef<any>): void {
    if (this.returnDateLabel.length === 0 || this.rentDateLabel.length === 0) {
      this.openModal(templateError);
    } else {
      this.reservationService.addReservation(new Rent(carId, +this.route.snapshot.paramMap.get('id'), this.rentDateLabel
        , this.returnDateLabel, false, false)).subscribe(reservation => {this.reservations.push(reservation);
        });
      this.openModal(templateSuccess);
    }
  }

  filterCars(rentDate: string, returnDate: string, templateError: TemplateRef<any>): void {
    if (returnDate.length > 0 && rentDate.length > 0 && new Date(rentDate).getTime() <= new Date(returnDate).getTime()) {
      this.rentDateLabel = rentDate;
      this.returnDateLabel = returnDate;
      this.reservations = this.reservations
        .filter(r => new Date(r.rentDate)
          .getTime() < new Date(returnDate).getTime() && new Date(r.returnDate).getTime() > new Date(rentDate).getTime());
      this.rents = this.rents
        .filter(r => new Date(r.rentDate)
          .getTime() < new Date(returnDate).getTime() && new Date(r.returnDate).getTime() > new Date(rentDate).getTime());
      this.cars = this.carsCopy.filter(c => new Date(c.servicePeriod).getTime() < new Date(rentDate)
        .getTime() || new Date(c.servicePeriod).getTime() > new Date(returnDate).getTime());
      for (let i = 0; i < this.reservations.length; i++) {
        this.cars = this.cars.filter(c => c.id !== this.reservations[i].idCar);
      }
      for (let i = 0; i < this.rents.length; i++) {
        this.cars = this.cars.filter(c => c.id !== this.rents[i].idCar);
      }
      this.getReservations();
      this.getRents();
    } else {
      this.openModal(templateError);
    }
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }
}

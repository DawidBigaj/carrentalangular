import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const customers = [
      {id: 11, name: 'Viki', surname: 'Hush', email: 'vhush0@hubpages.com', creditCardNumber: '3563875524554340', password: 'p1'},
      {id: 12, name: 'June', surname: 'Titheridge', email: 'jtitheridge1@blog.com', creditCardNumber: '630461812348718564', password: 'p2'},
      {id: 13, name: 'Waylin', surname: 'Mattiussi', email: 'wmattiussi2@mail.ru', creditCardNumber: '5641820617446712635', password: 'p3'},
      {id: 14, name: 'Aland', surname: 'Coster', email: 'acoster3@addtoany.com', creditCardNumber: '201916978677353', password: 'p4'},
      {id: 15, name: 'Hillier', surname: 'Geany', email: 'hgeany4@google.de', creditCardNumber: '5411031170822933', password: 'p5'},
      {id: 16, name: 'Munroe', surname: 'Scipsey', email: 'munroe586@vp.pl', creditCardNumber: '3587905148153856', password: 'p6'},
      {id: 17, name: 'Maximo', surname: 'Cuerdale', email: 'mcuerdale6@pen.io', creditCardNumber: '3536305524434958', password: 'p7'},
      {id: 18, name: 'Rosamund', surname: 'Favelle', email: 'rfavelle7@usatoday.com', creditCardNumber: '4041372319854671', password: 'p8'},
      {id: 19, name: 'Irma', surname: 'Karchowski', email: 'ikarchowski8@press.com', creditCardNumber: '3538283533007495', password: 'p9'},
      {id: 20, name: 'Virgie', surname: 'Lasslett', email: 'vlasslett9@andnoble.com', creditCardNumber: '374283298195058', password: 'p10'}
    ];
    const cars = [
      {
        id: 11,
        mark: 'Ford',
        model: 'Crown Victoria',
        description: 'none',
        colour: 'red',
        productionDate: '11/21/2012',
        retirementDate: '06/17/2027',
        mileage: 30738,
        servicePeriod: '05/11/2018'
      },
      {
        id: 12,
        mark: 'Pontiac',
        model: 'Daewoo Kalos',
        description: 'none',
        colour: 'red',
        productionDate: '01/21/2005',
        retirementDate: '02/24/2024',
        mileage: 78903,
        servicePeriod: '12/22/2018'
      },
      {
        id: 13,
        mark: 'Mercury',
        model: 'Grand Marquis',
        description: 'none',
        colour: 'white',
        productionDate: '10/28/2003',
        retirementDate: '08/13/2028',
        mileage: 27949,
        servicePeriod: '01/22/2019'
      },
      {
        id: 14,
        mark: 'BMW',
        model: '5 Series',
        description: 'none',
        colour: 'black',
        productionDate: '02/22/2003',
        retirementDate: '01/24/2025',
        mileage: 204613,
        servicePeriod: '05/28/2018'
      },
      {
        id: 15,
        mark: 'Ford',
        model: 'Stratus',
        description: 'none',
        colour: 'red',
        productionDate: '01/03/2013',
        retirementDate: '06/21/2025',
        mileage: 142645,
        servicePeriod: '12/01/2017'
      }
    ];
    const reservations = [
      {
        id: 11,
        idCustomer: 11,
        idCar: 11,
        rentDate: '04/14/2018',
        returnDate: '04/17/2018',
        gps: false,
        wifi: false,
      },
      {
        id: 12,
        idCustomer: 12,
        idCar: 12,
        rentDate: '04/14/2018',
        returnDate: '04/22/2018',
        gps: false,
        wifi: false,
      },
      {
        id: 13,
        idCustomer: 13,
        idCar: 13,
        rentDate: '04/20/2018',
        returnDate: '04/22/2018',
        gps: false,
        wifi: false,
      },
    ];
    const rents = [
      {
        id: 11,
        idCustomer: 14,
        idCar: 14,
        rentDate: '04/12/2018',
        returnDate: '04/21/2018',
        gps: false,
        wifi: false,
      },
      {
        id: 12,
        idCustomer: 15,
        idCar: 15,
        rentDate: '04/07/2018',
        returnDate: '04/15/2018',
        gps: false,
        wifi: false,
      },
      {
        id: 13,
        idCustomer: 12,
        idCar: 12,
        rentDate: '04/07/2018',
        returnDate: '04/12/2018',
        gps: false,
        wifi: false,
      },
    ];
    return {customers, cars, reservations, rents};
  }
}

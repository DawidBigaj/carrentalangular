import { TestBed } from '@angular/core/testing';
import { CarService } from './car.service';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Car} from './car';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('CarService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let carService: CarService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CarService]
    });

    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    carService = TestBed.get(CarService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  describe('#getCars', () => {
    let expectedCars: Car[];

    beforeEach(() => {
      carService = TestBed.get(CarService);
      expectedCars = [
        {
          id: 11,
          mark: 'Ford',
          model: 'Crown Victoria',
          description: 'none',
          colour: 'red',
          productionDate: '11/21/2012',
          retirementDate: '06/17/2027',
          mileage: '30738',
          servicePeriod: '05/11/2018'
        },
        {
          id: 12,
          mark: 'Pontiac',
          model: 'Daewoo Kalos',
          description: 'none',
          colour: 'red',
          productionDate: '01/21/2005',
          retirementDate: '02/24/2024',
          mileage: '78903',
          servicePeriod: '12/22/2018'
        }
      ] as Car[];
    });
    it('should return expected cars (called once)', () => {
      carService.getCars().subscribe(
        cars => expect(cars).toEqual(expectedCars, 'should return expected cars'),
        fail
      );
      const req = httpTestingController.expectOne(carService.carsUrl);
      expect(req.request.method).toEqual('GET');
    });
  });

  describe('#getCar', () => {
    let car: Car;

    beforeEach(() => {
      carService = TestBed.get(CarService);
      car = {
        id: 11,
        mark: 'Ford',
        model: 'Crown Victoria',
        description: 'none',
        colour: 'red',
        productionDate: '11/21/2012',
        retirementDate: '06/17/2027',
        mileage: '30738',
        servicePeriod: '05/11/2018'
      } as Car;
    });
    it('should return expected car (called once)', () => {
      carService.getCar(car.id).subscribe(
        c => expect(c).toEqual(car, 'should return expected car'),
        fail
      );
      const req = httpTestingController.expectOne(carService.carsUrl + '/' + car.id);
      expect(req.request.method).toEqual('GET');
    });

    it('should be OK returning no cars', () => {
      carService.getCar(12).subscribe(
        c => expect(c.id).toEqual(undefined, 'should have empty cars array'),
        fail
      );

      const req = httpTestingController.expectOne(carService.carsUrl + '/' + 12);
      expect(req.request.method).toEqual('GET');
    });
  });

  describe('#updateCar', () => {

    it('should update a car and return it', () => {

      const updateCar: Car = {
        id: 12,
        mark: 'Pontiac',
        model: 'Daewoo Kalos',
        description: 'none',
        colour: 'red',
        productionDate: '01/21/2005',
        retirementDate: '02/24/2024',
        mileage: '78903',
        servicePeriod: '12/22/2018'
      };

      carService.updateCar(updateCar).subscribe(
        data => expect(data).toEqual(updateCar, 'should return the car'),
        fail
      );

      const req = httpTestingController.expectOne(carService.carsUrl);
      expect(req.request.method).toEqual('PUT');
      expect(req.request.body).toEqual(updateCar);
    });
  });

  describe('#addCar', () => {

    it('should add a car and return it', () => {

      const car: Car = {
        id: 12,
        mark: 'Pontiac',
        model: 'Daewoo Kalos',
        description: 'none',
        colour: 'red',
        productionDate: '01/21/2005',
        retirementDate: '02/24/2024',
        mileage: '78903',
        servicePeriod: '12/22/2018'
      };

      carService.addCar(car).subscribe(
        data => expect(data).toEqual(car, 'should return the car'),
        fail
      );

      const req = httpTestingController.expectOne(carService.carsUrl);
      expect(req.request.method).toEqual('POST');
      expect(req.request.body).toEqual(car);
    });
  });

  describe('#deleteCar', () => {
    let cars: Car[];
    it('should delete a car', () => {

      cars = [
        {
          id: 11,
          mark: 'Ford',
          model: 'Crown Victoria',
          description: 'none',
          colour: 'red',
          productionDate: '11/21/2012',
          retirementDate: '06/17/2027',
          mileage: '30738',
          servicePeriod: '05/11/2018'
        },
        {
          id: 12,
          mark: 'Pontiac',
          model: 'Daewoo Kalos',
          description: 'none',
          colour: 'red',
          productionDate: '01/21/2005',
          retirementDate: '02/24/2024',
          mileage: '78903',
          servicePeriod: '12/22/2018'
        }
      ] as Car[];

      carService.deleteCar(cars[0]).subscribe();

      const req = httpTestingController.expectOne(carService.carsUrl + '/' + cars[0].id);
      expect(req.request.method).toEqual('DELETE');
      expect(req.request.body).toEqual(null);
    });
  });
});
